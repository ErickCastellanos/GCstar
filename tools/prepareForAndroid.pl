#!/usr/bin/perl

use strict;
use File::Copy;

my $baseDevDir = '/home/tian/Developpement';

my $baseLangDir = "$baseDevDir/GCstar/gcstar/lib/gcstar/GCLang/";
my $baseLangDestDir = "$baseDevDir/AndroidGCstar/res/values";

my $baseModelsDir = "$baseDevDir/GCstar/gcstar/lib/gcstar/GCModels/";
my $baseModelsDestDir = "$baseDevDir/AndroidGCstar/res/xml/";


chdir $baseLangDir;
my $type = 'GCModels';

foreach my $lang(glob('*'))
{
    next if $lang !~ /^[A-Z]{2}$/;
    my $destDir =  $baseLangDestDir.(($lang eq 'EN') ? '' : ('-'.(lc $lang)));
    print "MKDIR $destDir\n";
    mkdir $destDir or die "Cannot create $destDir : $!"
        if ! -d $destDir;

    open IN, "$lang/GCstar.pm" or die "Cannot open $lang/GCstar.pm : $!";
    open OUT, ">$destDir/generic.xml" or die "Error $!";
    print OUT '<?xml version="1.0" encoding="utf-8"?>
<resources>
';
    while (<IN>)
    {
        if (/(\s*Panel|ContextImgBack)/)
        {
            /(\w*)'?\s*=>\s*'(.*?)',/;
            print OUT "<string name=\"generic.$1\">$2</string>\n";
        }
    }
    print OUT '</resources>
';
    close IN;
    close OUT;

    chdir "$lang/$type";

    foreach my $model(glob('*'))
    {
        open IN, $model;
        (my $xml = lc $model) =~ s/gc([^.]*)\.pm/$1/;
        next if ($xml eq 'generic');
        print "Writing $model in $destDir/$xml.xml\n";
        open OUT, ">$destDir/$xml.xml" or die "Error $!";
        print OUT '<?xml version="1.0" encoding="utf-8"?>
<resources>
';
        while (<IN>)
        {
            if (/(\w*)'?\s*=>\s*'(.*?)',/)
            {
                print OUT "<string name=\"$xml.$1\">$2</string>\n";
            }
        }
        print OUT '</resources>
';
        close IN;
        close OUT;
    }
    chdir "../..";
}

chdir $baseModelsDir;
foreach my $model(glob('*.gcm'))
{
    (my $newName = lc $model) =~ s/^gc//;
    copy $model, "$baseModelsDestDir/$newName";
}
