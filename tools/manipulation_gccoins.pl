#!/usr/bin/perl
use strict;
use warnings;
use POSIX;
# permet le tri avec prise en compte des accents et des majuscules
use Unicode::Collate;
my $col = Unicode::Collate->new(level => 1);


# outil de manipulation d'une collection de monnaie GCstar (GCcoins)
# format XML.
# en entrée, le nom du fichier gcstar et de son chemin
# en sortie, le même fichier réécrit
# en sortie, fichier HTML de statistiques
# en sortie, fichier HTML de liste simple pour transformation epub
# en sortie, fichier TXT de liste des monnaies sans photo

# objectifs : 
# 1) trier les items sur le pays et le nom de la monnaie => pas fait
# 2) renuméroter les items et optimisation du maxID => fait
# 3) nettoyer les tags => fait
# 4) supprimer les lignes vides, caractères espace en trop, ...
# 5) vérification du fichier de données écrit par rapport à la feuille de style XSD
# 6) permettre de faire des listes, des statistiques (comptage), des exportations, ... => fait
# 7) encoder les caractères exotiques (fonction xml_quote) : => fait
#		" lors de l'écriture devient &quot;
#		& lors de l'écriture devient &amp;
# 8) GCstar semble avoir un bug dans les balises d'image
#		de temps en temps, le chemin est perdu, il y a :
#		 ../../disque2/gcstar/bin => il se rajoute à chaque enregistrement du fichier
#		nettoyer chemin des images (front, back et edge)  => fait
# 9) mettre un zéro devant les diamètres d'un chiffre : 9->09  => fait

# todo :
# construire la classe gccoin (collection de monnaie)
# utiliser les objets items (une pièce de monnaie)

# pour bien manipuler les caractères exotiques (en entrée et en sortie)
use open(IO => ':encoding(utf8)');
binmode(STDERR, ':utf8');
binmode(STDOUT, ':utf8');
binmode(STDIN, ':utf8');

# le fichier GCcoins contenant la collection de monnaie GCstar à analyser
my $fichier = "numismatiq.gcs";

# pour manipuler un fichier XML
# utilisation du module LibXML
use XML::LibXML;
my $parser = XML::LibXML->new();
# création de l'arbre XML en fonction du fichier lu
my $tree = $parser->parse_file($fichier);

# Racine du document XML
my $root = $tree->getDocumentElement;

# la balise collection correspond à la racine du fichier XML
# le type de collection dans la balise collection
# le type doit correspondre à une collection GCcoins
my $data_type = $root->getAttribute('type');

# si ce n'est pas le bon type, il vaut mieux tout arrêter
if ($data_type ne "GCcoins" ) {
	exit;
}

# le nombre d'items (de monnaie) dans la balise collection 
my $data_items = $root->getAttribute('items');

# la version de la collection GCstar dans la balise collection
my $data_version = $root->getAttribute('version');

# le numéro maximum attribué (pour la création du prochain item)
my $data_maxId;

  # Balise information
  foreach my $information ( $root->getElementsByTagName('information') ) {
	# Balise maxId
	foreach my $maxId ( $information->getElementsByTagName('maxId') ) {
		$data_maxId = $maxId->getFirstChild->getData;
	}
  }

# Création du fichier résultat en UTF8 (?)
# il est sensé être la reproduction fidèle du fichier en entrée
# avec des corrections si possible
# faire une comparaison avec MELD
my $FichierResultat = 'monnaies.gcs';
open( my $FhResultat, '>', $FichierResultat )
  or die("Impossible d'ouvrir le fichier $FichierResultat\n$!");

print {$FhResultat} '<?xml version="1.0" encoding="UTF-8"?>'."\n";
print {$FhResultat} '<collection type="'.$data_type.'" items="'.$data_items.'" version="'.$data_version.'">'."\n";
print {$FhResultat} ' <information>'."\n";
print {$FhResultat} '  <maxId>'.$data_items.'</maxId>'."\n";
print {$FhResultat} ' </information>'."\n";
# pourquoi 3 lignes vides ?
print {$FhResultat} ''."\n";
print {$FhResultat} ''."\n";
print {$FhResultat} ''."\n";

# variables de comptage pour les statistiques
my $nb_tags1 = 0;
my $nb_tags2 = 0;
my $nb_items = 0;
# table de hashage des pays
my %nb_pays;
# identifiant d'une pièce
my $id = 0;
# valeur d'achat de la collection
my $valeur = 0;
# valeur estimation catalogue 1 de la collection (dollars)
my $valeur1 = 0;
# valeur estimation catalogue 2 de la collection (euros)
my $valeur2 = 0;
# table de hashage des métaux
my %nb_metal;
# table de hashage des formes
my %nb_forme;
# table de hashage des diametres
my %nb_diametre;
# table de hashage des tags
my %nb_tags;
# table de hashage des emplacements
my %nb_emplacement;

# Balise item (monnaie)
my @item = $root->getElementsByTagName('item');

# Création du fichier de log
my $FichierLog = 'monnaies_log.txt';
open( my $FhLog, '>', $FichierLog )
  or die("Impossible d'ouvrir le fichier $FichierLog\n$!");

# Création du fichier des monnaies sans photo
my $FichierPhoto = 'monnaies_sans_photo.txt';
open( my $FhSPho, '>', $FichierPhoto )
  or die("Impossible d'ouvrir le fichier $FichierPhoto\n$!");

# pour voir le contenu du document XML chargé
#use Data::Dumper;
#print Dumper @item;

# ici le tri ?
# todo : tri par pays puis par nom de @item

# Création du fichier de la liste des monnaies brute avant tri et mise en forme
my $FichierListeB = 'liste_monnaie.txt';
open( my $FhListB, '>', $FichierListeB )
  or die("Impossible d'ouvrir le fichier $FichierListeB\n$!");

  # parcours des attributs de chaque item
  foreach my $monnaie ( @item ) {
	 my $data_gcsautoid = $monnaie->getAttribute('gcsautoid');
	 my $data_name = $monnaie->getAttribute('name');
	 my $data_picture = $monnaie->getAttribute('picture');
	 my $data_back = $monnaie->getAttribute('back');
	 my $data_country = $monnaie->getAttribute('country');
	 my $data_year = $monnaie->getAttribute('year');
	 my $data_quantity = $monnaie->getAttribute('quantity');
	 my $data_years_of_coinage = $monnaie->getAttribute('years_of_coinage');
	 my $data_value = $monnaie->getAttribute('value');
	 my $data_currency = $monnaie->getAttribute('currency');
	 my $data_type = $monnaie->getAttribute('type');
	 my $data_mint = $monnaie->getAttribute('mint');
	 my $data_mint_mark = $monnaie->getAttribute('mint_mark');
	 my $data_mintmaster = $monnaie->getAttribute('mintmaster');
	 my $data_mintmaster_mark = $monnaie->getAttribute('mintmaster_mark');
	 my $data_city = $monnaie->getAttribute('city');
	 my $data_city_letter = $monnaie->getAttribute('city_letter');
	 my $data_series = $monnaie->getAttribute('series');
	 my $data_metal = $monnaie->getAttribute('metal');
	 my $data_weight = $monnaie->getAttribute('weight');
	 my $data_diameter = $monnaie->getAttribute('diameter');
	 my $data_depth = $monnaie->getAttribute('depth');
	 my $data_number = $monnaie->getAttribute('number');
	 my $data_form = $monnaie->getAttribute('form');
	 my $data_axis = $monnaie->getAttribute('axis');
	 my $data_edge_type = $monnaie->getAttribute('edge_type');
	 my $data_edge1 = $monnaie->getAttribute('edge1');
	 my $data_edge2 = $monnaie->getAttribute('edge2');
	 my $data_edge3 = $monnaie->getAttribute('edge3');
	 my $data_edge4 = $monnaie->getAttribute('edge4');
	 my $data_condition = $monnaie->getAttribute('condition');
	 my $data_buyed = $monnaie->getAttribute('buyed');
	 my $data_found = $monnaie->getAttribute('found');
	 my $data_bring = $monnaie->getAttribute('bring');
	 my $data_gift = $monnaie->getAttribute('gift');
	 my $data_location = $monnaie->getAttribute('location');
	 my $data_added = $monnaie->getAttribute('added');
	 my $data_catalogue1 = $monnaie->getAttribute('catalogue1');
	 my $data_number1 = $monnaie->getAttribute('number1');
	 my $data_estimate = $monnaie->getAttribute('estimate');
	 my $data_catalogue2 = $monnaie->getAttribute('catalogue2');
	 my $data_number2 = $monnaie->getAttribute('number2');
	 my $data_estimate2 = $monnaie->getAttribute('estimate2');
	 my $data_website = $monnaie->getAttribute('website');
	 my $data_webPage = $monnaie->getAttribute('webPage');
	 my $data_price = $monnaie->getAttribute('price');
	 my $data_favourite = $monnaie->getAttribute('favourite');

	 my $data_tags1 = "";
	if ($monnaie->getAttribute('tags')) {
		$data_tags1 = $monnaie->getAttribute('tags');
		# comptage des tags incorrects
		$nb_tags1 +=  1 ;
	}

	my $data_head = "";
	# balise head
	foreach my $head ( $monnaie->getElementsByTagName('head') ) {
		# attention quand la balise est vide
		if ($head->hasChildNodes) { 
			$data_head = $head->getFirstChild->getData;
		}
	}

	my $data_tail = "";
	# balise tail
	foreach my $tail ( $monnaie->getElementsByTagName('tail') ) {
		if ($tail->hasChildNodes) { 
			$data_tail = $tail->getFirstChild->getData;
		}
	}

	my $data_edge = "";
	# balise edge
	foreach my $edge ( $monnaie->getElementsByTagName('edge') ) {
		if ($edge->hasChildNodes) { 
			$data_edge = $edge->getFirstChild->getData;
		}
	}

	my $data_comments = "";
	# balise comments
	foreach my $comment ( $monnaie->getElementsByTagName('comments') ) {
		if ($comment->hasChildNodes) { 
			$data_comments = $comment->getFirstChild->getData;
		}
	}

	my $data_history = "";
	# balise history
	foreach my $history ( $monnaie->getElementsByTagName('history') ) {
		if ($history->hasChildNodes) { 
			$data_history = $history->getFirstChild->getData;
		}
	}

	my $data_references = "";
	# balise references
	foreach my $references ( $monnaie->getElementsByTagName('references') ) {
		if ($references->hasChildNodes) { 
			$data_references = $references->getFirstChild->getData;
		}
	}

	# en fin de fichier les tags sont gérés avec une balise
	my $data_tags2 = "";
	if ($monnaie->getElementsByTagName('tags')) {
		# balise tags
		foreach my $tags ( $monnaie->getElementsByTagName('tags') ) {
			if ($tags->hasChildNodes) {
				foreach my $line ( $tags->getElementsByTagName('line') ) {
					if ($line->hasChildNodes) {
						foreach my $col ( $line->getElementsByTagName('col') ) {
							$data_tags2 = $col->getFirstChild->getData;
						}
		# comptage des tags corrects
		$nb_tags2 += 1;
					}
				}
			}
		}
	}

# comptage des monnaies
$nb_items += 1;
# alimentation table pays et comptage des pays
$nb_pays{$data_country}++;
# alimentation table métal et comptage des métaux
$nb_metal{$data_metal}++;
# alimentation table forme et comptage des formes
$nb_forme{$data_form}++;
# alimentation table diamètre et comptage des diamètres (arrondi au chiffre supérieur)
# problème quand pas numérique (remplacement de la virgule par le point)
$data_diameter =~ s/\,/\./ ;
if ($data_diameter eq "") {
	$data_diameter = 00;
}
# et rajout du zéro à gauche
$nb_diametre{ajout_zero(ceil($data_diameter))}++;
# alimentation table tags et comptage des tags
$nb_tags{$data_tags1}++;
$nb_tags{$data_tags2}++;
# alimentation table emplacement et comptage des emplacements
$nb_emplacement{$data_location}++;

# écriture de la monnaie dans le fichier résultat
# renuméroter de 1 à $data_items
$id++;
print {$FhResultat} ' <item '."\n";
print {$FhResultat} '  gcsautoid="'.$id.'"'."\n";
print {$FhResultat} '  name="'.&xml_quote($data_name).'"'."\n";

# vérification du bon chemin des images (numismatiq_pictures/nom_image.jpg)
# sinon, modifier le chemin et écrire dans un log l'image à déplacer
if ( $data_picture =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' front '.$data_picture."\n";
		$data_picture = "numismatiq_pictures/".$2;
	}
}
print {$FhResultat} '  front="'.$data_picture.'"'."\n";

# si la monnaie n'a pas de photo
if ( $data_picture eq "" ) {
	print {$FhSPho} ' '.&xml_quote($data_name).' gcsautoid="'.$id.'"'."\n";

}


if ( $data_back =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' back '.$data_back."\n";
		$data_back = "numismatiq_pictures/".$2;
	}
}
print {$FhResultat} '  back="'.$data_back.'"'."\n";

# si la monnaie n'a pas de photo
if ( $data_back eq "" ) {
	print {$FhSPho} '  '.&xml_quote($data_name).' gcsautoid="'.$id.'"'."\n";

}

print {$FhResultat} '  country="'.&xml_quote($data_country).'"'."\n";
print {$FhResultat} '  year="'.$data_year.'"'."\n";
print {$FhResultat} '  quantity="'.$data_quantity.'"'."\n";
print {$FhResultat} '  years_of_coinage="'.$data_years_of_coinage.'"'."\n";
print {$FhResultat} '  value="'.$data_value.'"'."\n";
print {$FhResultat} '  currency="'.&xml_quote($data_currency).'"'."\n";
print {$FhResultat} '  type="'.$data_type.'"'."\n";
print {$FhResultat} '  mint="'.$data_mint.'"'."\n";
print {$FhResultat} '  mint_mark="'.$data_mint_mark.'"'."\n";
print {$FhResultat} '  mintmaster="'.&xml_quote($data_mintmaster).'"'."\n";
print {$FhResultat} '  mintmaster_mark="'.$data_mintmaster_mark.'"'."\n";
print {$FhResultat} '  city="'.$data_city.'"'."\n";
print {$FhResultat} '  city_letter="'.$data_city_letter.'"'."\n";
print {$FhResultat} '  series="'.&xml_quote($data_series).'"'."\n";
print {$FhResultat} '  metal="'.&xml_quote($data_metal).'"'."\n";
print {$FhResultat} '  weight="'.$data_weight.'"'."\n";
print {$FhResultat} '  diameter="'.$data_diameter.'"'."\n";
print {$FhResultat} '  depth="'.$data_depth.'"'."\n";
print {$FhResultat} '  number="'.$data_number.'"'."\n";
print {$FhResultat} '  form="'.$data_form.'"'."\n";
print {$FhResultat} '  axis="'.$data_axis.'"'."\n";
print {$FhResultat} '  edge_type="'.&xml_quote($data_edge_type).'"'."\n";

if ( $data_edge1 =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' edge1 '.$data_edge1."\n";
		$data_edge1 = "";
	}
}

print {$FhResultat} '  edge1="'.$data_edge1.'"'."\n";

if ( $data_edge2 =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' edge2 '.$data_edge2."\n";
		$data_edge2 = "";
	}
}
print {$FhResultat} '  edge2="'.$data_edge2.'"'."\n";

if ( $data_edge3 =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' edge3 '.$data_edge3."\n";
		$data_edge3 = "";
	}
}
print {$FhResultat} '  edge3="'.$data_edge3.'"'."\n";

if ( $data_edge4 =~ /^(.*)\/(.*\.jpg)/ ) {
	if (($1 ne "numismatiq_pictures") and ($1 ne ".numismatiq_pictures")) {
		print {$FhLog} $data_country." ".$id." ".$data_name.' edge4 '.$data_edge4."\n";
		$data_edge4 = "";
	}
}
print {$FhResultat} '  edge4="'.$data_edge4.'"'."\n";


if ($data_condition eq "") {
	$data_condition = "60";
}
if ($data_condition eq "FDC") {
	$data_condition = "70";
}
if ($data_condition eq "SPL") {
	$data_condition = "60";
}
if ($data_condition eq "SUP") {
	$data_condition = "55";
}
if ($data_condition eq "TTB") {
	$data_condition = "45";
}
if ($data_condition eq "TB") {
	$data_condition = "30";
}
if ($data_condition eq "B") {
	$data_condition = "12";
}

print {$FhResultat} '  condition="'.$data_condition.'"'."\n";

if ( $data_buyed eq "" ) {
	$data_buyed = "0";
}
print {$FhResultat} '  buyed="'.$data_buyed.'"'."\n";

if ( $data_found eq "" ) {
	$data_found = "0";
}
print {$FhResultat} '  found="'.$data_found.'"'."\n";
if ( $data_bring eq "" ) {
	$data_bring = "0";
}
print {$FhResultat} '  bring="'.$data_bring.'"'."\n";
if ( $data_gift eq "" ) {
	$data_gift = "0";
}
print {$FhResultat} '  gift="'.$data_gift.'"'."\n";
print {$FhResultat} '  location="'.&xml_quote($data_location).'"'."\n";
if ($data_added eq "") {
	$data_added = "10/10/2010";
}
if ($data_price eq "") {
	$data_price = "0";
}
print {$FhResultat} '  added="'.$data_added.'"'."\n";
print {$FhResultat} '  catalogue1="'.$data_catalogue1.'"'."\n";
print {$FhResultat} '  number1="'.$data_number1.'"'."\n";
if ($data_estimate eq "") {
	$data_estimate = $data_price;
}
# cumul pour la valeur en dollar du World Coins
$valeur1 = $valeur1 + $data_estimate;

print {$FhResultat} '  estimate="'.$data_estimate.'"'."\n";
print {$FhResultat} '  catalogue2="'.$data_catalogue2.'"'."\n";
print {$FhResultat} '  number2="'.$data_number2.'"'."\n";
if ($data_estimate2 eq "") {
	$data_estimate2 = $data_price;
}
# cumul pour la valeur en euro de la conversion dollar -> euro
$valeur2 = $valeur2 + $data_estimate2;

print {$FhResultat} '  estimate2="'.$data_estimate2.'"'."\n";
print {$FhResultat} '  website="'.&xml_quote($data_website).'"'."\n";
print {$FhResultat} '  webPage="'.&xml_quote($data_webPage).'"'."\n";

# valeur totale de la collection
# problème lorsque $data_price est vide = ""
if ($data_price eq "") {
	$data_price = 0;
}
$valeur = $valeur + $data_price;
print {$FhResultat} '  price="'.$data_price.'"'."\n";
if ( $data_favourite eq "" ) {
	$data_favourite = "0";
}
print {$FhResultat} '  favourite="'.$data_favourite.'"'."\n";
print {$FhResultat} ' >'."\n";
print {$FhResultat} '  <head>'.&xml_quote($data_head).'</head>'."\n";
print {$FhResultat} '  <tail>'.&xml_quote($data_tail).'</tail>'."\n";
print {$FhResultat} '  <edge>'.&xml_quote($data_edge).'</edge>'."\n";
print {$FhResultat} '  <comments>'.&xml_quote($data_comments).'</comments>'."\n";
print {$FhResultat} '  <history>'.&xml_quote($data_history).'</history>'."\n";
print {$FhResultat} '  <references>'.&xml_quote($data_references).'</references>'."\n";

print {$FhResultat} '  <tags>';
if ($data_tags2 or $data_tags1) {
	print {$FhResultat} '<line>';
	print {$FhResultat} '<col>'.&xml_quote($data_tags1).&xml_quote($data_tags2).'</col>';
	print {$FhResultat} '</line>';
}
print {$FhResultat} '</tags>'."\n";
print {$FhResultat} ' </item>'."\n";

# constitution de la liste des monnaies brute avec séparateur ;
if ($data_country ne "") {
	print {$FhListB} ''.&xml_quote($data_country).';';
	print {$FhListB} ''.$data_number1.';'.$data_front.';'.$data_back.';';
	print {$FhListB} ''.&xml_quote($data_name).''."\n";
}

	# fin items
  }

# fin du fichier résultat
print {$FhResultat} '</collection>'."\n";
close($FhResultat);
close($FhLog);
close($FhSPho);
close($FhListB);

# tri du fichier de la liste brute ?
open(MYINPUTFILE, "<liste_monnaie.txt"); # open for input
open(MYOUTPUTFILE, ">liste_monnaie_trie.txt"); # open for output
my(@lines) = <MYINPUTFILE>; # read file into list

# le tri ne prend pas en compte les accents !!!
@lines = sort(@lines); # sort the list
my($line);
foreach $line (@lines) # loop thru list
 {
 print MYOUTPUTFILE "$line"; # print in sort order
 }
close(MYINPUTFILE);
close(MYOUTPUTFILE);

# conservation du pays précédent pour le titre 1
my $ancien_pays = "";
my $premP = 0;
my $dernP = 0;
# conservation du km précédent pour le p
my $ancien_km = "";
my $premK = 0;
my $dernK = 0;

# Création du fichier de la liste des monnaies pour epub (comversion HTML avec calibre)
# ouverture du fichier brute trié
my $FichierLB = 'liste_monnaie_trie.txt';
open(FLB, '<', $FichierLB)
  or die("Impossible d'ouvrir le fichier $FichierLB\n$!");

my $FichierListe = 'liste_monnaie.html';
open( my $FhList, '>', $FichierListe )
  or die("Impossible d'ouvrir le fichier $FichierListe\n$!");
print {$FhList} "<html>\n";
print {$FhList} "<head>\n";
print {$FhList} "<title>Liste des Monnaies</title>\n";
print {$FhList} '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'."\n";
print {$FhList} '<link rel="stylesheet" href="liste.css" type="text/css" media="all" />'."\n";
print {$FhList} "</head>\n";
print {$FhList} "<body>\n";
print {$FhList} "<div>\n";

# boucle de lecture de la liste brute
my $ligne = "";
while( defined( $ligne = <FLB> ) ) {
	chomp($ligne);
	# split de la ligne avec ; pour récupérer les champs
	my @tab = split( /;/, $ligne, -1 );
	# première ligne du pays courant
	if (($ancien_pays ne $tab[0]) and ($premP == 0)) {
		$premP = 1;
	}
	# pas la première ligne du pays courant
	if (($ancien_pays eq $tab[0]) and ($premP == 1)) {
		$premP = 0;
	}
	# première ligne du KM courant
	if (($ancien_km ne $tab[1]) and ($premK == 0)) {
		$premK = 1;
	}
	# plus la première ligne du KM courant
	if (($ancien_km eq $tab[1]) and ($premK == 1)) {
		$premK = 0;
	}
	# on ne veut pas les jetons ni la france
if (($tab[0] ne 0) and ($tab[0] !~ /Jeton/) and ($tab[0] !~ /France/)) {
	# constitution de la liste des monnaies en HTML pour conversion epub
	if ($premP == 1) {
		print {$FhList} '<h1>'.$tab[0].'</h1>'."\n";
		$ancien_pays = $tab[0];
	}
	if ($premK == 1) {
		print {$FhList} '<p>'.$tab[1].'<img src="'.$tab[2].'" /><img src="'.$tab[3].'" /></p>'."\n".'';
		$ancien_km = $tab[1];
	}
	print {$FhList} '<li>'.$tab[4].'</li>'."\n";
  }
}  # fin boucle liste brute

# fin du fichier liste des monnaies
print {$FhList} "</div>\n";
print {$FhList} "</body></html>\n";
close($FhList);

# fermeture du fichier brute
close(FLB);

# Création du fichier de statistiques HTML
my $FichierStats = 'monnaies_statistiques.html';
open( my $FhStats, '>', $FichierStats )
  or die("Impossible d'ouvrir le fichier $FichierStats\n$!");
print {$FhStats} "<html>\n";
print {$FhStats} "<head>\n";
print {$FhStats} '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'."\n";
print {$FhStats} "</head>\n";
print {$FhStats} "<body>\n";

print {$FhStats} "<p>nbtags1=", $nb_tags1, "</p>\n";
print {$FhStats} "<p>nbtags2=", $nb_tags2, "</p>\n";
# nombre de clés dans la table de hashage des pays
my $nbr = keys %nb_pays;
print {$FhStats} "<p>nombre de pays=", $nbr, "</p>\n";

# détail du nombre de monnaies par pays
my @pays = $col->sort( keys(%nb_pays) );
# construction d'un tableau statistique pays/nb monnaie
print {$FhStats} "<table><th>Pays</th><th>nb monnaies</th>\n";
foreach my $v (@pays) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_pays{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

# détail du nombre de monnaies par métal
my @metal = $col->sort( keys(%nb_metal) );
# construction d'un tableau statistique métal/nb monnaie
print {$FhStats} "<table><th>M&eacute;tal</th><th>nb monnaies</th>\n";
foreach my $v (@metal) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_metal{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

# détail du nombre de monnaies par forme
my @forme = $col->sort( keys(%nb_forme) );
# construction d'un tableau statistique forme/nb monnaie
print {$FhStats} "<table><th>Forme</th><th>nb monnaies</th>\n";
foreach my $v (@forme) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_forme{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

# détail du nombre de monnaies par diamètre
my @diametre = $col->sort( keys(%nb_diametre) );
# construction d'un tableau statistique diamètre/nb monnaie
print {$FhStats} "<table><th>Diam&egrave;tre</th><th>nb monnaies</th>\n";
foreach my $v (@diametre) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_diametre{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

# détail du nombre de monnaies par tags
my @tags = $col->sort( keys(%nb_tags) );
# construction d'un tableau statistique tags/nb monnaie
print {$FhStats} "<table><th>Tags</th><th>nb monnaies</th>\n";
foreach my $v (@tags) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_tags{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

# nombre de clés dans la table de hashage des emplacements
my $nbc = keys %nb_emplacement;
print {$FhStats} "<p>nombre de classeurs=", $nbc, "</p>\n";
# détail du nombre de monnaies par emplacement
my @emplacement = $col->sort( keys(%nb_emplacement) );
# construction d'un tableau statistique emplacement/nb monnaie
print {$FhStats} "<table><th>Emplacement</th><th>nb monnaies</th>\n";
foreach my $v (@emplacement) {
   print {$FhStats} "<tr><td>", $v, "</td><td>", $nb_emplacement{$v}, "</td></tr>\n";
}
print {$FhStats} "</table>\n";

print {$FhStats} "<p>dernier id=", $id, "</p>\n";
print {$FhStats} "<p>nombre de pi&egrave;ces=", $nb_items, "</p>\n";
# arrondi à 2 chiffres après la virgule
$valeur = sprintf ("%0.2f", $valeur);
print {$FhStats} "<p>valeur d'achat de la collection=", $valeur, "</p>\n";
# arrondi à 2 chiffres après la virgule
$valeur1 = sprintf ("%0.2f", $valeur1);
print {$FhStats} "<p>valeur d'estimation de la collection=", $valeur1, " dollars</p>\n";
# arrondi à 2 chiffres après la virgule
$valeur2 = sprintf ("%0.2f", $valeur2);
print {$FhStats} "<p>valeur d'estimation de la collection=", $valeur2, " euros</p>\n";

# fin du fichier statistiques
print {$FhStats} "</body></html>\n";

close($FhStats);

sub ajout_zero {
	my(@args) = @_;
	my $t = $args[0];
	$t = sprintf ("%0*d", 2, $t);
	return($t) ;	
}

sub xml_quote {
	my(@args) = @_;
	my $t = $args[0];
	# permet de remplacer les & par des &amp; les < et les > aussi ainsi que les "
	# toujours commencer par le &
	$t =~ s/&/&amp;/gm;
	$t =~ s/</&lt;/gm;
	$t =~ s/>/&gt;/gm;
	$t =~ s/"/&quot;/gm;
	return($t) ;
}
