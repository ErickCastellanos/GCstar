#!/usr/bin/perl

my $BASE_DIR=$ENV{HOME}.'/tmp/gcstar';

use File::Path;
use File::Copy;
use LWP::UserAgent;


my $CONFIG_SUBDIR = 'config';

my $REPOSITORY_BASEURL = 'http://download.gna.org/gcstar/gcstar-';
my $REPOSITORY_SUFFIX  = '.tar.gz';

my $SVN_SNAPSHOT = 'http://svn.gna.org/daily/gcstar-snapshot.tar.gz';

my $LOCAL_COPY = 'gcstar.tar.gz';

# Preparation

mkpath $BASE_DIR;
chdir $BASE_DIR;
mkdir $CONFIG_SUBDIR;
$ENV{XDG_CONFIG_HOME} = $BASE_DIR.'/'.$CONFIG_SUBDIR;

if ($ARGV[0] eq '-d')
{
    my $version = $ARGV[1];
    my $url;
    if (($version eq 'CVS') || ($version eq 'SVN'))
    {
        $url = $SVN_SNAPSHOT;
    }
    else
    {
        $url = $REPOSITORY_BASEURL.$version.$REPOSITORY_SUFFIX;
    }
    print "Downloading\n";
    my $browser = LWP::UserAgent->new;
    $browser->get($url, ':content_file' => $LOCAL_COPY);
    if (! -f $LOCAL_COPY)
    {
        print "Unable to download version $version\n";
        print "Please check it exists at $url\n";
        exit 1;
    }
    print "Extracting\n";
    `tar zxf $LOCAL_COPY`;
    print "Copying\n";
    if (($version eq 'CVS') || ($version eq 'SVN'))
    {
        `cp -r gcstar/gcstar/* .`;
    }
    else
    {
        `cp -r gcstar/* .`;
    }
    print "Cleaning\n";
    rmtree 'gcstar';
    unlink 'gcstar.tar.gz';
}

chdir 'bin';
exec 'perl ./gcstar';

