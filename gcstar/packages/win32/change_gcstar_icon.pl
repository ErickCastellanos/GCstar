use Win32::Exe;
$exe = Win32::Exe->new('gcstar.exe');
my $info = $exe->get_version_info;
print qq($_ = $info->{$_}\n) for (sort keys(%$info));
$exe->update( icon => '../share/gcstar/icons/GCstar.ico',
              info => [ 'FileDescription=GCstar, Personal Collections Manager', 'FileVersion=1.7.2',
                        'ProductName=GCstar', 'ProductVersion=1.7.2',
                        'Comments=http://www.gcstar.org', 'LegalCopyright=GNU GPL'] );
my $info_check = $exe->get_version_info;
print qq($_ = $info_check->{$_}\n) for (sort keys(%$info));
