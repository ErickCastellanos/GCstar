#! bash
#
# testPlugins.sh
#
# script to execute some GCstar commands to check that some plugins are still working
#

if [ -x ${GCSTAR+x} ]
then
    echo "Environment variables to set : GCSTAR GCSTAR_LOG TEMP"
    exit
fi

# location of GCstar main program
#GCSTAR=
# directory for GCstar log
#GCSTAR_LOG=
# direction for temporay files and cached files
#TEMP=
# normal test => 1, debug of the test script => 3
GCS_DEBUG_PLUGIN_PHASE=3

alias perl="$PERL_BIN"

count_file()
{
    if test -f $1
    then
        extension="${1##*.}"
        case $extension in
            "gcs")
                echo $(grep -E 'title=|name=' $1 | wc) $1
                ;;
            "html")
                echo $(grep '<h2>' $1 | wc) $1
                ;;
            *)
                echo $(cat $1 | wc) $1
                ;;
        esac
    else
        echo "#### ERROR #### " $1
    fi
}

merge_files()
{
    echo "=============================================================================="
    echo '=====     Merging ' $1 $2
    echo ''
  
    # collection type
    C=$2
    # root for test files
    R=Test_$1
    # import type
    I=$3
    
    shift; shift ; shift
    
    if [ "$1" = "-f" ]
    then
        shift
        FILES=$*
    else
        FILES=""
        for i in $*
        do
            FILES="$FILES ${R}_$i.gcs"
        done
    fi

    perl "$GCSTAR" -x --collection $C -o ${R}_all.gcs -i $I $FILES
    perl "$GCSTAR" -x -e CSV \
         -o ${R}_all_csv.csv ${R}_all.gcs
    
    echo "______________________________________________________________________________" 
    echo "Merged file"
    
    count_file ${R}_all.gcs
    count_file ${R}_all_csv.csv
 
    if test -f ${R}_all.gcs
    then
        echo $(grep -E 'title=|name=' ${R}_all.gcs | wc) ${R}_all.gcs
        echo $(cat ${R}_all_csv.csv | wc) ${R}_all_csv.csv
    else
        echo "#### ERROR #### " ${R}_all.gcs
    fi
	echo ""
	echo ""
	echo ""
}

test_site() {
    echo "=============================================================================="
    echo '=====     ' $3 $5
    echo ''
  
    # collection type
    C=$2
    # root for test files
    T=Test_$1
  
    rm -r ${T}_$3_* 2>&1 > /dev/null

	sleep 1
    # test download from web site
    perl "$GCSTAR" -x --collection $C -w "$4" -o ${T}_$3.gcs --download "$5"
    cp $GCSTAR_LOG/gcstar.log ${T}_$3_$RANDOM.log

    count_file ${T}_$3.gcs
    
    # test saving a gcs file
    perl "$GCSTAR" -x --collection $C \
         -o ${T}_$3_new.gcs ${T}_$3.gcs
    count_file  ${T}_$3_new.gcs
  
    # test export
    perl "$GCSTAR" -x --collection $C -e HTML \
         --exportprefs "template=>Shelf,title=>$1" \
         -o ${T}_$3.html ${T}_$3.gcs
    #perl "$GCSTAR" -x -e External  --exportprefs "zip=>1" \
    #     -o ${T}_$3.gcz  ${T}_$3.gcs
    perl "$GCSTAR" -x -e CSV \
         -o ${T}_$3_csv.csv ${T}_$3.gcs
    perl "$GCSTAR" -x -e Tellico \
         -o ${T}_$3.tc ${T}_$3.gcs
  
    # test download and export
    perl "$GCSTAR" -x --collection $C -w "$4" -e HTML \
         --exportprefs "template=>Shelf,title=>My Collection,collection=>tmp.gcs" \
         -o ${T}_$3_new.html --download "$5"
  
    # test import from Tellico
    perl "$GCSTAR" -x --collection $C -i Tellico \
         -o ${T}_$3_Tellico.gcs ${T}_$3.tc
    count_file ${T}_$3_Tellico.gcs
  
    echo "______________________________________________________________________________" 
    echo "GCS files"
    count_file ${T}_$3.gcs
    count_file ${T}_$3_new.gcs
    count_file ${T}_$3_Tellico.gcs
    echo "______________________________________________________________________________"
    echo "HTML files" 
    count_file ${T}_$3.html
    count_file ${T}_$3_new.html
    echo "______________________________________________________________________________"
    echo "CSV file" 
    count_file ${T}_$3_csv.csv
    echo "________________________________________"
    echo "Tellico file" 
    count_file ${T}_$3.tc
    echo ""
    echo ""
}

collection_header() {
    echo "=============================================================================="
    echo "=====  $1  ====="
    echo "=============================================================================="
    echo ""
}

test_Books() {
    collection_header "BOOKS                                                           "
    test_site Books GCbooks "AmazonCA"     "Amazon (CA)"  "Harry Potter"
    test_site Books GCbooks "AmazonDE"     "Amazon (DE)"  "Das Jesus Video"
    test_site Books GCbooks "AmazonFR"     "Amazon (FR)"  "A la recherche du temps perdu"
    test_site Books GCbooks "AmazonUK"     "Amazon (UK)"  "David Copperfield"
    test_site Books GCbooks "AmazonUS"     "Amazon (US)"  "East of Eden"
    test_site Books GCbooks "Bdphile"      "Bdphile"      "Aquablue"
    test_site Books GCbooks "Bokkilden"    "Bokkilden"    "Harry Potter" # https://www.bokkilden.no/enkeltSok.do?enkeltsok=harry+potter&search=&rom=MP
    test_site Books GCbooks "Chapitre.com" "Chapitre.com" "Harry Potter"
    test_site Books GCbooks "GBooks"       "Google Books" "Harry Potter"
    test_site Books GCbooks "NooSFere"     "NooSFere"     "Harry Potter"

    # plugins to check
    test_site Books GCbooks "BibliotekaNarodowa" "Biblioteka Narodowa" "Harry Potter" # http://alpha.bn.org.pl/search*pol

    test_site Books GCbooks "Le-Livre" "Le-Livre" "Harry Potter"
    test_site Books GCbooks "FnacFR"   "Fnac (FR)"   "A la recherche du temps perdu"
    # test_site Books GCbooks "DoubanBook" "Douban Book" "Harry Potter" # require a change in plugin name

    # plugins that don't work but may be fixed (website available)
    #test_site Books GCbooks "Adlibris (FI)" "Adlibris (FI)" "xxx" https://www.adlibris.com/fi/haku?q=harry+potter
    #test_site Books GCbooks "Adlibris (SV)" "Adlibris (SV)" "xxx" https://www.adlibris.com/se/sok?q=harry+potter
    #test_site Books GCbooks "Bol" "#" "Harry Potter" http://www.mondadoristore.it/search/?g=Harry+potter&crc=600&bld=15&swe=N&escal=S&accum=N
    #test_site Books GCbooks "Buscape" "#Buscape" "Harry Potter" http://www.buscape.com.br/livros/harry-potter
    #test_site Books GCbooks "Casadelibro" "#Casadelibro" "Harry Potter" https://www.casadellibro.com/libros-harry-potter/151
    #test_site Books GCbooks "Fnac (PT)" "# Fnac (PT)" "Harry Potter" https://www.fnac.pt/SearchResult/ResultList.aspx?SCat=0%211&Search=harry+potter&sft=1&sa=0
    #test_site Books GCbooks "InternetBookShop" "#InternetBookShop" "Harry Potter"
    #test_site Books GCbooks "ISBNdb" "#ISBNdb" "9789544464912" http://isbndb.com/search/all?query=9789544464912
    #test_site Books GCbooks "Mediabooks" "#Mediabooks" "Harry Potter"
    #test_site Books GCbooks "Merlin" "#Merlin" "Harry Potter"
    #test_site Books GCbooks "NUKat" "#NUKat" "Harry Potter"
    #test_site Books GCbooks "Saraiva" "#Saraiva" "Harry Potter"

    # plugin for sites that closed
    # Alapage InternetBokHandeln LiberOnWeb Mareno
    
    merge_files Books GCBooks GCstar \
                AmazonCA AmazonDE AmazonFR AmazonUK AmazonUS Bdphile FnacFR GBooks \
                NooSFere BibliotekaNarodowa Bokkilden Chapitre.com Le-Livre
}

test_Films() {
    collection_header "FILMS                                                           "
    test_site Films GCfilms Allmovie          "Allmovie"                "Avatar"
    test_site Films GCfilms Allocine          "Allocine.fr"             "Avatar"
    test_site Films GCfilms AmazonDE          "Amazon (DE)"             "Avatar"
    test_site Films GCfilms AmazonFR          "Amazon (FR)"             "Avatar"
    test_site Films GCfilms AmazonUK          "Amazon (UK)"             "Avatar"
    test_site Films GCfilms AmazonUS          "Amazon (US)"             "Avatar"
    test_site Films GCfilms Animator          "Animator"                "Ava"    
    test_site Films GCfilms AnimatorEN        "AnimatorEN"              "PEK" 
    test_site Films GCfilms Animeka           "Animeka.com"             "Avatar"
    test_site Films GCfilms CSFD              "CSFD.cz"                 "Avatar"
    test_site Films GCfilms Douban            "豆瓣 - Douban"              "Avatar"
    test_site Films GCfilms DVDEmpire         "DVDEmpire (EN)"          "Avatar"
    test_site Films GCfilms DVDPost           "DVDPost.be"              "Avatar"   
    test_site Films GCfilms FilmAffinityEN    "Film affinity (EN)"      "Avatar"
    test_site Films GCfilms FilmAffinityES    "Film affinity (ES)"      "Avatar"
    test_site Films GCfilms FilmUP            "FilmUP"                  "Avatar"
    test_site Films GCfilms FilmWeb           "FilmWeb"                 "Avatar"
    test_site Films GCfilms InternetBookshop  "IBS - Internet Bookshop" "Avatar" 
    test_site Films GCfilms IMDb              "IMDb"                    "Avatar"
    test_site Films GCfilms MovieCovers       "MovieCovers.com"         "Avatar"
    test_site Films GCfilms MovieMeter        "MovieMeter.nl"           "Avatar"
    test_site Films GCfilms OdeonHU           "Odeon.hu"                "Avatar"
    test_site Films GCfilms OFDd              "OFDb.de"                 "Avatar"

    # plugins that don't work but may be fixed (website available)
    #test_site Films GCfilms AniDB "AniDB" "Avatar"
    #test_site Films GCfilms AnimeNfo "AnimeNfo Anime" "Avatar"
    #test_site Films GCfilms BeyazPerde "Beyaz Perde" "Avatar"
    #test_site Films GCfilms CinemaClock "CinemaClock.com" "Avatar"
    #test_site Films GCfilms Cinemotions "Cinemotions.com" "Avatar"
    #test_site Films GCfilms Dicshop "Discshop.se" "Avatar"
    #test_site Films GCfilms Kinopoisk "Kinopoisk" "Avatar"
    #test_site Films GCfilms NasheKino "NasheKino" "Avatar"
    #test_site Films GCfilms Onet "Onet" "Avatar"
    #test_site Films GCfilms PortHU "port.hu" "Avatar"
    #test_site Films GCfilms Stopklatka "Stopklatka" "Avatar"
    #test_site Films GCfilms TheMovieDB "The Movie DB" "Avatar"
    #test_site Films GCfilms TheMovieDBDE "The Movie DB (DE)" "Avatar"
    #test_site Films GCfilms TheMovieDBES "The Movie DB (ES)" "Avatar"
    #test_site Films GCfilms TheMovieDBFR "The Movie DB (FR)" "Avatar"

    # plugin for sites that closed
    # Alapage Alpacine CartelesMetropoliGlobal CartelesPeliculas CulturaliaNet Mediadis MonsieurCinema

    merge_files Films GCfilms GCstar \
                Allmovie Allocine AmazonDE AmazonFR AmazonUK AmazonUS \
                Animator AnimatorEN Animeka CSFD Douban DvDEmpire DVDPost \
                FilmAffinityEN FilmAffinityES FilmUP FilmWeb InternetBookshop \
                IMDb MovieCovers MovieMeter OdeonHU OFDd
}

test_Comics() {
    collection_header "COMICS                                     "
    test_site Comics GCcomics Bdphile        "Bdphile"         "Aquablue"
    test_site Comics GCcomics Bedetheque     "Bedetheque"      "Aquablue"
    test_site Comics GCcomics ComicBookDB    "Comic Book DB"   "Before Watchmen"
    test_site Comics GCcomics MangaSanctuary "Manga-Sanctuary" "Mushishi"

    merge_files Comics GCcomics GCstar \
                BDphile Bedetheque ComicBookDB MangaSanctuary
}

test_BuildingToys() {
    collection_header "BUILDING TOYS                                                   "

    test_site BuildingToys GCbuildingtoys Brickset "Brickset" "Skywalker"
}

test_Musics() {
    collection_header "MUSICS                                                          "

    test_site Music GCmusics MusicBrainz "MusicBrainz" "Eliminator"

    #test_site Music GCmusics DoubanMusic "Doubanmusic" "Eliminator" # requires a patch to GCDoubanmusic.pm

    #test_site Music GCmusics Discogs "Discogs" "Eliminator" # not working
}

test_VideoGames() {
    collection_header "VIDEO GAMES                                                     "

    test_site VideoGames GCgames AmazonCA     "Amazon (CA)"   "Uncharted"
    test_site VideoGames GCgames AmazonDE     "Amazon (DE)"   "Uncharted"
    test_site VideoGames GCgames AmazonFR     "Amazon (FR)"   "Uncharted"
    test_site VideoGames GCgames AmazonJP     "Amazon (JP)"   "Uncharted"
    test_site VideoGames GCgames AmazonUK     "Amazon (UK)"   "Uncharted"
    test_site VideoGames GCgames AmazonUS     "Amazon (US)"   "Uncharted"
    test_site VideoGames GCgames Gamespot     "GameSpot"      "Ultima"
    test_site VideoGames GCgames JeuxVideoCom "jeuxvideo.com" "Uncharted"
    test_site VideoGames GCgames MobyGames    "MobyGames"     "Uncharted"
    
    # plugins that don't work but may be fixed (website available)
    # test_site VideoGames GCgames JeuxVideoFR  "jeuxvideo.fr"  "Uncharted"
    # test_site VideoGames GCgames NextGame     "NextGame"      "Uncharted"

    # plugin for sites that closed
    # Alapage, DicoDuNet, Ludus
    # test_site VideoGames GCgames TheLegacy    "TheLegacy"     "Keen"

    merge_files VideoGames GCgames GCstar \
                AmazonCA AmazonDE AmazonFR AmazonJP AmazonUK AmazonUS \
                Gamespot JeuxVideoCom MobyGames
}

test_BoardGames() {
    collection_header "BOARD GAMES                                                     "

    test_site BoardGames GCboardgames BoardGameGeek "Board Game Geek" "Carcassonne"
    test_site BoardGames GCboardgames TricTrac      "Tric Trac"       "Carcassonne"

    # test_site BoardGames GCboardgames ReservoirJeux "Reservoir Jeux"  "Carcassonne"   

    merge_files BoardGames GCboardgames GCstar \
                BoardGameGeek TricTrac
}

test_Gadgets() {
    collection_header "GADGETS                                                         "

    test_site Gadgets GCgadgets AmazonCA_Clock      "Amazon (CA)" "4905524962031" # _Clock
    test_site Gadgets GCgadgets AmazonCA_Earphones  "Amazon (CA)" "4905524727685" # _Earphones
    test_site Gadgets GCgadgets AmazonCA_Disk       "Amazon (CA)" "0763649091173" # _Disk
    test_site Gadgets GCgadgets AmazonCA_Camera_bis "Amazon (CA)" "0074101037722" # _Camera_bis

    merge_files Gadgets_AmazonCA GCgadgets GCstar Clock Earphones Disk Camera_bis 

    test_site Gadgets GCgadgets AmazonDE_Clock      "Amazon (DE)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonDE_Earphones  "Amazon (DE)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonDE_Camera     "Amazon (DE)" "4548736014367" # Camera
    test_site Gadgets GCgadgets AmazonDE_Disk       "Amazon (DE)" "0763649091173" # Disk

    merge_files Gadgets_AmazonDE GCgadgets GCstar Clock Earphones Camera Disk Book

    test_site Gadgets GCgadgets AmazonFR_Clock      "Amazon (FR)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonFR_Earphones  "Amazon (FR)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonFR_Camera     "Amazon (FR)" "4548736014367" # Camera
    test_site Gadgets GCgadgets AmazonFR_Disk       "Amazon (FR)" "0763649091173" # Disk

    merge_files Gadgets_AmazonFR GCgadgets GCstar Clock Earphones Camera Disk

    test_site Gadgets GCgadgets AmazonUK_Clock      "Amazon (UK)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonUK_Earphones  "Amazon (UK)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonUK_Camera     "Amazon (UK)" "4548736014367" # Camera
    test_site Gadgets GCgadgets AmazonUK_Disk       "Amazon (UK)" "0763649091173" # Disk

    merge_files Gadgets_AmazonUK GCgadgets GCstar Clock Earphones Camera Disk

    test_site Gadgets GCgadgets AmazonUS_Clock      "Amazon (US)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonUS_Earphones  "Amazon (US)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonUS_Disk       "Amazon (US)" "0763649091173" # Disk
    test_site Gadgets GCgadgets AmazonUS_Camera_bis "Amazon (US)" "0074101037722" # Camera bis

    merge_files Gadgets_AmazonUS GCgadgets GCstar Clock Earphones Disk Camera_bis

    test_site Gadgets GCgadgets UPCitemDB_Clock      "UPC item DB" "4905524962031" # Clock
    test_site Gadgets GCgadgets UPCitemDB_Earphones  "UPC item DB" "4905524727685" # Earphones
    test_site Gadgets GCgadgets UPCitemDB_Camera     "UPC item DB" "4548736014367" # Camera
    test_site Gadgets GCgadgets UPCitemDB_Disk       "UPC item DB" "0763649091173" # Disk
    test_site Gadgets GCgadgets UPCitemDB_Camera_bis "UPC item DB" "0074101037722" # Camera bis

    merge_files Gadgets_UPCitemDB GCgadgets GCstar Clock Earphones Camera Disk Camera_bis

    merge_files Gadgets GCgadgets GCstar \
                AmazonCA_all AmazonDE_all AmazonFR_all AmazonQC_all \
                AmazonUK_all AmazonUS_all UPCitemDB_all
}

test_Coins_Search() {
    FileName=Test_Coin_${1// /_}.gcs
    echo $FileName
    perl "$GCSTAR" -x --collection GCcoins -w "fr.numista" -o $FileName --download "$1"
    cat $FileName >> Tests_Coins.gcs
    wc $FileName
}

test_Coins() {
    # manual tests
    collection_header "COINS"
    
    rm Tests_Coins.gcs
    test_Coins_Search "300 Euros"
    test_Coins_Search "277 Euros"
    test_Coins_Search "Hercule"
    test_Coins_Search "10 Escudos"
    test_Coins_Search "Mont Saint Michel"
    test_Coins_Search "Mickey"
    test_Coins_Search "Napoleon"
    test_Coins_Search "jeton touristique monnaie de paris"
    test_Coins_Search "doudou"
    test_Coins_Search "Obama"
    test_Coins_Search "10 Euros"
    test_Coins_Search "25 Euros"
    test_Coins_Search "Taiwan"
    test_Coins_Search "grande muraille de chine"
    test_Coins_Search "tricentenaire connecticut"
    test_Coins_Search "napoleon III tete nue"
    test_Coins_Search "comte de toulouse"
    test_Coins_Search "afghani Amanullah"
    test_Coins_Search "thai national bank"
    test_Coins_Search "Muhammad al Amin"
    test_Coins_Search "vatican"
    test_Coins_Search "salva"
    test_Coins_Search "grosso"
    test_Coins_Search "lirot"
    test_Coins_Search "holstein"
    test_Coins_Search "prince regent"
    test_Coins_Search "albert II effigie"
    test_Coins_Search "louisiane"
    test_Coins_Search "krugerrand"
    test_Coins_Search "Margrethe"

    merge_files Coins GCcoins GCstar -f Test_Coin_*.gcs
}

test_export () {
    extension="${2,,}"
    perl "$GCSTAR" -x -c $1 -e $2 -o Test_$3.$extension Test_$3.gcs    
}

test_Exports () {
    test_export TVseries CSV Series_Dvdb
    test_export TVepisodes CSV Episodes_Dvdb
    test_export Buildingtoys CSV Construction
    test_export Wines CSV Wines
    test_export Software CSV Software
    test_export Minicars CSV Minicars
    test_export Periodicals CSV Periodicals
    test_export Coins CSV Coins
}

# test a web site
 if  [ $# -eq 5 ]
 then
        test_site $1 $2 $3 "$4" "$5"
        exit
 fi
 
 # test for a collection type
 if  [ $# -eq 1 ]
 then
        test_$1
        exit
 fi

# test everything

test_Coins
test_BoardGames
test_BuildingToys
test_Books
test_Comics
test_Films
test_Gadgets
test_Musics
test_VideoGames

test_Exports


